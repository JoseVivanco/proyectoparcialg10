/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.util.*;
import interfaz.*;
import entidades.*;
import usuarios.*;

/**
 *
 * @author Brank
 */
public class Establecimiento {

    private static Integer codigoIncremental = 1;
    private final Integer codigo;
    private String ciudad;
    private String sector;
    private String direccion;
    private String[] telefonos;
    private String horarios;
    private Empresa empresa;
    private ArrayList<Promocion> promociones;

    public Establecimiento(String ciudad, String sector, String direccion, String[] telefonos, String horarios, Empresa empresa) {
        this.codigo = codigoIncremental++;
        this.ciudad = ciudad;
        this.sector = sector;
        this.direccion = direccion;
        this.telefonos = telefonos;
        this.horarios = horarios;
        this.empresa = empresa;
        this.promociones = new ArrayList<>();
    }

    public static Establecimiento comprobarEstablecimiento(Empresa empresa, String direccion) {
        Establecimiento establecimiento = null;
        for (Establecimiento est : empresa.getEstablecimientos()){
            if (est.getDireccion().equalsIgnoreCase(direccion)){
                establecimiento = est;
                break;
            }
        }                
        return establecimiento;
    }

    public static void registrarEstablecimiento(Empresa empresa) {
        System.out.println("Ingrese direccion del establecimiento :");
        String nDireccion = Utilidades.ingresoString();
        if (Establecimiento.comprobarEstablecimiento(empresa, nDireccion) == null) {
            System.out.println("Ingrese ciudad :");
            String nCiudad = Utilidades.ingresoString();
            System.out.println("Ingrese sector :");
            String nSector = Utilidades.ingresoString();
            System.out.println("Ingrese teléfonos :");
            String[] nTelefonos = Utilidades.ingresoString().split(",");
            System.out.println("Ingrese horarios :");
            String nHorario = Utilidades.ingresoString();
            Establecimiento nuevoEstablecimiento = new Establecimiento(nCiudad, nSector, nDireccion, nTelefonos, nHorario, empresa);
            empresa.getEstablecimientos().add(nuevoEstablecimiento);
            empresa.getEstablecimientos().add(nuevoEstablecimiento);
            System.out.println("Establecimiento agregado con éxito");
        } else {
            System.out.println("El establecimiento ya existe");
        }
    }

    /* GETTERS & SETTERS */
    public Integer getCodigo() {
        return codigo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String[] getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String[] telefonos) {
        this.telefonos = telefonos;
    }

    public String getHorarios() {
        return horarios;
    }

    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public ArrayList<Promocion> getPromociones() {
        return promociones;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

}
